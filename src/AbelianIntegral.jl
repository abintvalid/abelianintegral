
module AbelianIntegral

using Base, DataStructures, DifferentialEquations, IntervalArithmetic, ApproxFun, RigorousFourier, Reexport, TypedPolynomials, LinearAlgebra

include("./HamiltonianStructures.jl")

@reexport using .HamiltonianStructures

using IntervalArithmetic: Interval, @round, @round_up, @round_down
using ApproxFun: (..)
using RigorousFourier: FS, IFS, default_usepow2
using .HamiltonianStructures: X, Y, PolyType

export ovalapprox_iterative, ovalapprox_TA, abelianintegral_trapez, newtonball, ovalvalid, abelianintegral_valid



# iterative solvers to approximate the level curve using the Hamiltonian dynamical system

# maximum integration time
Tmax = 1000

# for iterative methods, specify the type for numerical computations (e.g., Float64 or BigFloat)

# using the Hamiltonian in Cartesian coordinates
function ovalapprox_iterative_cartesian(::Type{T}, H::AbstractHamiltonian, ov::OvalFamily, r::Real; prec=1e-10) where {T<:Real}
    found_first_return = false
    x0 = T(ov.x0)
    y0 = T(ov.y0)
    α = T(ov.α)
    rmin = T(ov.rmin)
    rmax = T(ov.rmax)
    or = ov.orient
    function stop_condition(u, t, integrator)
        if t == 0
            return 1
        else
            cross = cos(α) * (u[2] - y0) - sin(α) * (u[1] - x0)
            return or * cross
        end
    end
    function affect!(integrator)
        u = integrator.u
        ur = cos(α) * (u[1] - x0) + sin(α) * (u[2] - y0)
        if rmin < ur < rmax
            found_first_return = true
            terminate!(integrator)
        end
    end
    cb = ContinuousCallback(stop_condition, affect!, abstol=prec)
    u0 = [x0 + T(r) * cos(α), y0 + T(r) * sin(α)]
    function hamilt!(du,u,p,t)
        du[1] = T(H.vx(u[1],u[2]))
        du[2] = T(H.vy(u[1],u[2]))
    end
    prob = ODEProblem(hamilt!, u0, (T(0), T(Tmax)))
    u = solve(prob, reltol=prec, abstol=prec, callback=cb)
    if !found_first_return
        error("first return not detected")
    end
    tend = u.t[end]
    x(t::T) = u(t)[1]
    y(t::T) = u(t)[2]
    dx(t::T) = T(H.vx(x(t),y(t)))
    dy(t::T) = T(H.vy(x(t),y(t)))
    return tend, x, y, dx, dy
end

# using the Hamiltonian in polar coordinates
function ovalapprox_iterative_polar(::Type{T}, H::AbstractHamiltonian, ov::OvalFamily, r::Real; prec=1e-10, polardata=false) where {T<:Real}
    α = T(ov.α)
    x0 = T(ov.x0)
    y0 = T(ov.y0)
    rmin = T(ov.rmin)
    rmax = T(ov.rmax)
    argend = 2*T(π)
    found_first_return = false
    stop_condition(u, t, integrator) = abs(u[2]) - argend
    function affect!(integrator)
        u = integrator.u
        if rmin < u[1] < rmax
            found_first_return = true
            terminate!(integrator)
        end
    end
    cb = ContinuousCallback(stop_condition, affect!, abstol=prec)
    u0 = [T(r), T(0)]
    function hamilt!(du,u,p,t)
        c = cos(u[2] + α)
        s = sin(u[2] + α)
        x = x0 + u[1] * c
        y = y0 + u[1] * s
        P = T(H.vx(x,y))
        Q = T(H.vy(x,y))
        du[1] = c * P + s * Q
        du[2] = (c * Q - s * P) / u[1]
    end
    prob = ODEProblem(hamilt!, u0, (T(0), T(Tmax)))
    u = solve(prob, reltol=prec, abstol=prec, callback=cb)
    if !found_first_return
        error("first return not detected")
    end
    tend = u.t[end]
    ρ(t::T) = u(t)[1]
    θ(t::T) = u(t)[2]
    x(t::T) = x0 + ρ(t) * cos(θ(t) + α)
    y(t::T) = y0 + ρ(t) * sin(θ(t) + α)
    dx(t::T) = T(H.dv(x(t),y(t)))
    dy(t::T) = T(H.dv(x(t),y(t)))
    if polardata
        return tend, x, y, dx, dy, ρ, θ
    else
        return tend, x, y, dx, dy
    end
end

# using the Hamiltonian with dρ/dθ ODE
function ovalapprox_iterative_odepolar(::Type{T}, H::AbstractHamiltonian, ov::OvalFamily, r::Real; prec=1e-10, polardata=false) where {T<:Real}
    α = T(ov.α)
    x0 = T(ov.x0)
    y0 = T(ov.y0)
    rmin = T(ov.rmin)
    rmax = T(ov.rmax)
    tend = 2*T(π)
    u0 = T(r)
    or = ov.orient
    found_first_return = false
    function hamilt(u,p,t)
        c = cos(α + or * t)
        s = sin(α + or * t)
        xx = x0 + u[1] * c
        yy = y0 + u[1] * s
        P = T(H.vx(xx,yy))
        Q = T(H.vy(xx,yy))
        du = or * u * (c * P + s * Q) / (c * Q - s * P)
        return du
    end
    prob = ODEProblem(hamilt, u0, (T(0), tend))
    u = solve(prob, reltol=prec, abstol=prec)
    ρ(t::T) = u(t)
    x(t::T) = x0 + ρ(t) * cos(α + or * t)
    y(t::T) = y0 + ρ(t) * sin(α + or * t)
    dth(t::T) = or * (cos(α + or * t) * T(H.vy(x(t),y(t))) - sin(α + or * t) * T(H.vx(x(t),y(t))))
    dx(t::T) = ρ(t) * T(H.vx(x(t),y(t))) / dth(t)
    dy(t::T) = ρ(t) * T(H.vy(x(t),y(t))) / dth(t)
    if polardata
        return tend, x, y, dx, dy, ρ
    else
        return tend, x, y, dx, dy
    end
end

function ovalapprox_iterative(::Type{T}, H::AbstractHamiltonian, ov::OvalFamily, r::Real; prec=1e-10, method="polar") where {T<:Real}
    ovalapprox_iterative_function = getfield(@__MODULE__, Symbol("ovalapprox_iterative_" * method))
    return ovalapprox_iterative_function(T, H, ov, r, prec=prec)
end

# apply at most "newtoniterations" Newton-Raphon iterations starting from (x,y) to converge to the oval H==h along the straight line going throw (x,y) and directed by the gradient of H at that point
# stops when defect is below "maxdefect"
function ovalapprox_newton_pt(H::AbstractHamiltonian, h::T, x::T, y::T; maxdefect=1e-30, newtoniterations=10) where {T<:Real}

    # check cclass >= 1
    if H.cclass < 1
        error("Potential function must have first derivatives")
    end

    # construct transversal direction
    ux = T(H.dx(x,y))
    uy = T(H.dy(x,y))
    δ = sqrt(ux^2 + uy^2)
    ux = ux / δ
    uy = uy / δ

    # Newton-Raphson iterations
    s = T(∞) # defect
    iter = 0
    while abs(s) > maxdefect && iter < newtoniterations
        # compute defect
        diff = ux * T(H.dx(x,y)) + uy * T(H.dy(x,y))
        s = (h - T(H(x,y))) / diff
        # update x and y
        x = x + s * ux
        y = y + s * uy
        iter = iter + 1
    end

    # warning if maxdefect not reached
    if abs(s) > maxdefect
        @warn("Maximum number of Newton-Raphson iterations reached without expected defect being reached.")
    end

    return x, y
end


# specifying radius on oval family instead of level h
function ovalapprox_newton_pt(H::AbstractHamiltonian, ov::OvalFamily, r::T, x::T, y::T; maxdefect=1e-30, newtoniterations=10) where {T<:Real}
    h = ovallevel(H, ov, r)
    ovalapprox_newton_pt(H, h, x, y, maxdefect=maxdefect, newtoniterations=newtoniterations)
end


# compute TAs x and y for level curve in Cartesian coordinates
# use an iterative method (specified by "itermethod") to compute initial approximation (using precision "iterativeprec")
# and refine with at most "newtoniterations" scalar Newton-Raphson iterations on the standard Fourier grid, until defect "maxdefect" is reached
# if "finalfft" is false, then return the sample values at Fourier grid instead of a TA
function ovalapprox_TA(::Type{T}, H::AbstractHamiltonian, ov::OvalFamily, r::Real; iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-30, finalfft=true, denoising=10., usepow2=default_usepow2) where {T<:Real}

    # corresponding level of H
    h = T(ovallevel(H, ov, r))

    # compute initial approximation with specified iterative method
    tend, fx, fy, _, _ = ovalapprox_iterative(T, H, ov, r, prec=iterprec, method=itermethod)

    # sample values on the Fourier grid
    n = RigorousFourier.default_len
    n = usepow2 ? nextpow(2, n) : n
    tvals = RigorousFourier.grid(T, len=n)
    TWOPI = 2*T(π)
    xvals = [fx(t/TWOPI*tend) for t=tvals]
    yvals = [fy(t/TWOPI*tend) for t=tvals]

    # denoising by regularization
    cutfreq(c::T) = abs(c) < denoising*iterprec ? T(0) : c
    FST = FS(T)
    xfreq = cutfreq.(ApproxFun.transform(FST, xvals))
    yfreq = cutfreq.(ApproxFun.transform(FST, yvals))
    xvals = ApproxFun.itransform(FST, xfreq)
    yvals = ApproxFun.itransform(FST, yfreq)

    # apply Newton-Raphson iterations if required
    if newtoniterations > 0
        for i = 1:n
            xvals[i], yvals[i] = ovalapprox_newton_pt(H, T(h), xvals[i], yvals[i], maxdefect=maxdefect, newtoniterations=newtoniterations)
        end
    end

    # convert samples values into TA if required
    if finalfft
        TAx = TA(ApproxFun.transform(FST, xvals))
        TAy = TA(ApproxFun.transform(FST, yvals))
        return TAx, TAy
    else
        return xvals, yvals
    end

end



# compute Abelian integral using the trapezoidal rule
# NOTE: the trapezoidal rule converges exponentially fast for periodic functions
# x(t) and y(t) are given by vectors of values on the Fourier equispaced grid
function abelianintegral_trapez(H::AbstractHamiltonian, pert::AbstractPerturbation, x::Vector{T}, y::Vector{T}) where {T<:Real}

    n = length(x)
    if length(y) != n
        error("values of x and y must be given on an equispaced Fourier grid of same number of points")
    end

    # compute derivatives of x and y on the grid using FFT and iFFT
    FST = FS(T)
    TAdx = TA(ApproxFun.transform(FST, x))'
    resize!(TAdx, n, pad=true)
    dx = ApproxFun.itransform(FST, coefficients(TAdx))
    TAdy = TA(ApproxFun.transform(FST, y))'
    resize!(TAdy, n, pad=true)
    dy = ApproxFun.itransform(FST, coefficients(TAdy))

    # perturbations and integrating factor on the grid
    wx = (w -> T.(w)).(broadcast(pert.wx, x, y))
    wy = (w -> T.(w)).(broadcast(pert.wy, x, y))
    σ = T.(broadcast(H.σ, x, y))

    # values of the integrand on the grid
    I = σ .* (wx .* dy .- wy .* dx)

    # trapezoidal rule
    return (2*T(π)*sum(I))/n

end

function abelianintegral_trapez(::Type{T}, H::AbstractHamiltonian, ov::OvalFamily, pert::AbstractPerturbation, r::Real; iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-30, denoising=1., usepow2=default_usepow2) where {T<:Real}
    x, y = ovalapprox_TA(T, H, ov, r, iterprec=iterprec, itermethod=itermethod, newtoniterations=newtoniterations, maxdefect=maxdefect, denoising=denoising, finalfft=false, usepow2=usepow2)
    return abelianintegral_trapez(H, pert, x, y)
end


## validation of the oval

# generic function to detect contracting ball in a Newton-like validation
# d = (upper bound of) distance bewteen x0 and T(x0)
# λ : r::T -> λ(r)::T upper bound of the Lipschitz ratio of operator T on B(x0,r)
function newtonball(d::T, λ; initradius=T(1), ratioprec=0.01) where {T<:Real}

    initinterval = ((T(0),λ(T(0))), (T(∞),T(∞)))
    S = Stack{typeof(initinterval)}()
    push!(S, initinterval)

    f_up(r,λr) = @round_up(@round_up(@round_up(T(1) - λr) * r) - d)
    f_down(r,λr) = @round_down(@round_down(@round_down(T(1) - λr) * r) - d)

    while !isempty(S)

        # current range for r to handle
        ((rl,λl), (rh,λh)) = pop!(S)

        # stop subdivision
        if (λh - λl) / λh <= ratioprec && λh < 1 && f_down(rh,λh) >= 0
            return (rh,λh)

        # needs further subdivision
        elseif (λh == T(∞) || (λh - λl) / λh > ratioprec) && λl < 1 && f_up(rh,λl) >= 0
            # current interval necessarily contains a root
            if λh < 1 && f_down(rh,λh) >= 0
                empty!(S)
            end
            # subdivide interval
            if rh < T(∞)
                rm = (rl + rh) / 2
            elseif rl > 0
                rm = 2 * rl
            else
                rm = initradius
            end
            λm = λ(rm)
            push!(S, ((rm,λm),(rh,λh)))
            push!(S, ((rl,λl),(rm,λm)))
        end

    end

    error("Unable to find a contracting ball")
end

function ovalvalid(H::AbstractHamiltonian, ov::OvalFamily, r::Real, x::TA{T}, y::TA{T}, usepow2=default_usepow2) where {T<:Real}

    # check cclass >= 2
    if H.cclass < 2
        error("Potential function must be of cclass at least 2")
    end

    # level h
    r = Interval{T}(r)
    h = ovallevel(H, ov, r)

    # compute TAs ux, uy and ψ to define the transverse vector field and the Newton-like operator T
    FST = FS(T)
    N = RigorousFourier.default_len
    N0 = usepow2 ? nextpow(2, N) : N
    xcfs = coefficients(resize!(TA(copy(coefficients(x))), N0, pad=true))
    xvals = ApproxFun.itransform(FST, xcfs)
    ycfs = coefficients(resize!(TA(copy(coefficients(y))), N0, pad=true))
    yvals = ApproxFun.itransform(FST, ycfs)
    uxvals = T.(broadcast(H.dx, xvals, yvals))
    uyvals = T.(broadcast(H.dy, xvals, yvals))
    δvals = sqrt.(uxvals .^2 .+ uyvals .^2)
    uxvals = uxvals ./ δvals
    uyvals = uyvals ./ δvals
    ψvals = [1/δvals[i] for i=1:length(δvals)]
    x = RTA(x)
    y = RTA(y)
    ux = resize!(TA(ApproxFun.transform(FST, uxvals)))
    uy = resize!(TA(ApproxFun.transform(FST, uyvals)))
    ψ = resize!(TA(ApproxFun.transform(FST, ψvals)))

    # bound Lipschitz ratio of T
    s = ψ * (h - H(x,y))
    d = wnorm(s)
    l0 = 1 - ψ * (ux * H.dx(x,y) + uy * H.dy(x,y))
    λ0 = wnorm(l0)
    b = wnorm(RTA(ψ))
    c = wnorm(RTA(ux)^2 + RTA(uy)^2)
    xbound = wnorm(x)
    ybound = wnorm(y)

    # function that computes the Lipschitz ratio of T over B(0,r)
    function λ(r::T)
        xr = (Interval{T}(xbound) + r) * Interval{T}(-1,1)
        yr = (Interval{T}(ybound) + r) * Interval{T}(-1,1)
        l1 = Interval{T}(c) * (abs(H.dxx(xr,yr)) + abs(H.dxy(xr,yr)) + abs(H.dyy(xr,yr)))
        λ1 = l1.hi
        return @round_up(λ0 + @round_up(b * @round_up(r * λ1)))
    end
    # find contracting ball
    (rmax, λmax) = newtonball(d, λ)
    ϵ = @round_up(d / @round_down(T(1) - λmax))

    ## check initial conditions
    # TODO

    ## check winding number of the curve
    # check that tube doesn't cross center of oval
    x0 = Interval{T}(ov.x0)
    y0 = Interval{T}(ov.y0)
    xx0 = x - x0
    yy0 = y - y0
    r2 = xx0^2 + yy0^2
    r2ϵ = r2 - (Interval{T}(ϵ)*Interval{T}(c))^2
    if !is_positive(r2ϵ)
        error("Tube crosses center of oval")
    end
    dx = RTA(x.pol')
    dy = RTA(y.pol')
    # compute and check index
    ind = sum((xx0 * dy - yy0 * dx) / r2) / (2 * Interval{T}(π))
    if diam(ind) >= T(1) || ! (T(ov.orient) ∈ ind)
        error("Curve does not have expected index")
    end

    return ϵ, ux, uy

end


function abelianintegral_valid(H::AbstractHamiltonian, pert::AbstractPerturbation, x::TA{T}, y::TA{T}, ε::T, ux::TA{T}, uy::TA{T}) where {T<:Real}

    n = pert.dim
    dx = RTA(ITA(x)')
    dy = RTA(ITA(y)')
    x = RTA(x)
    y = RTA(y)
    dux = RTA(ITA(ux)')
    duy = RTA(ITA(uy)')
    ux = RTA(ux)
    uy = RTA(uy)

    # compute rigorously integral along approximate curve
    σ = H.σ(x,y)
    wx = pert.wx(x,y)
    wy = pert.wy(x,y)
    I = sum.([σ * (wx[i] * dy - wy[i] * dx) for i=1:n])

    # compute error bounds
    a = pert.green(x,y)
    A = Interval{T}.(wnorm.(a))
    B1 = wnorm(dx * uy - dy * ux)
    B2 = wnorm(dux * uy - duy * ux)
    δ = (2 * Interval{T}(π) * Interval{T}(ε) * (2 * Interval{T}(B1) + Interval{T}(ε) * Interval{T}(B2))) * A

    return I + Interval{T}(-1,1) * δ

end


function abelianintegral_valid(::Type{T}, H::AbstractHamiltonian, ov::OvalFamily, pert::AbstractPerturbation, r::Real; iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-30, denoising=1., usepow2=default_usepow2) where {T<:Real}

    x, y = ovalapprox_TA(T, H, ov, r, itermethod=itermethod, iterprec=iterprec, newtoniterations=newtoniterations, finalfft=true, denoising=denoising, maxdefect=maxdefect)
    ε, ux, uy = ovalvalid(H, ov, r, x, y)
    I = abelianintegral_valid(H, pert, x, y, ε, ux, uy)
    return I

end


end # module

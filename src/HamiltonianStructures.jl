
module HamiltonianStructures

using TypedPolynomials, IntervalArithmetic, ApproxFun, RigorousFourier

using RigorousFourier: FS

export AbstractHamiltonian, FunctionHamiltonian, PolynomialHamiltonian, OvalFamily, ovallevel, AbstractPerturbation, FunctionPerturbation, PolynomialPerturbation


## types for Hamiltonian

abstract type AbstractHamiltonian end

(H::AbstractHamiltonian)(x,y) = H.pot(x,y)


# type for Hamiltonian defined with functions

mutable struct FunctionHamiltonian <: AbstractHamiltonian

    # the potential function R^2 → R -- can be directly called by the name of the Hamiltonian instance
    pot::Function

    # continuity class (0, 1 or 2)
    cclass::Int

    # derivatives of pot, if cclass >= 1
    dx::Function
    dy::Function

    # second derivatives of pot, if cclass >= 2
    dxx::Function
    dxy::Function
    dyy::Function

    # vector field vx and vy : R^2 → R
    vx::Function
    vy::Function

    # integrating factor σ : R^2 → R, that is:
    # σ * vx = - dy
    # σ * vy =   dx
    σ::Function

    # inner constructors

    # C0 constructor
    function FunctionHamiltonian(pot::Function, cclass::Int, vx::Function, vy::Function, σ::Function)
        if cclass == 0
            foo(_) = nothing
            return new(pot, cclass, foo, foo, foo, foo, foo, vx, vy, σ)
        elseif cclass == 1
            error("potential function of cclass 1 must be given with first derivatives dx and dy")
        elseif class == 2
            error("potential function of cclass 2 must be given with first and second derivatives dx, dy, dxx, dxy, dyy")
        else
            error("cclass must be 0, 1 or 2")
        end
    end

    # C1 constructor
    function FunctionHamiltonian(pot::Function, cclass::Int, dx::Function, dy::Function, vx::Function, vy::Function, σ::Function)
        if cclass == 1
            foo(_) = nothing
            return new(pot, cclass, dx, dy, foo, foo, foo, vx, vy, σ)
        elseif cclass == 0
            error("too many arguments given for a potential function of cclass 0")
        elseif class == 2
            error("potential function of cclass 2 must be given with first and second derivatives dx, dy, dxx, dxy, dyy")
        else
            error("cclass must be 0, 1 or 2")
        end
    end

    # C2 constructor
    function FunctionHamiltonian(pot::Function, cclass::Int, dx::Function, dy::Function, dxx::Function, dxy::Function, dyy::Function, vx::Function, vy::Function, σ::Function)
        if cclass == 2
            return new(pot, cclass, dx, dy, dxx, dxy, dyy, vx, vy, σ)
        elseif cclass == 0
            error("too many arguments given for a potential function of cclass 0")
        elseif cclass == 1
            error("too many arguments given for a potential function of cclass 1")
        else
            error("cclass must be 0, 1 or 2")
        end
    end

end

# type for Hamiltonian defined with explicit polynomials / rational functions

@polyvar X Y

# polynomials or rational functions
PolyType = Union{TypedPolynomials.AbstractPolynomialLike,TypedPolynomials.RationalPoly}

mutable struct PolynomialHamiltonian <: AbstractHamiltonian

    # the potential function R^2 → R -- can be directly called by the name of the Hamiltonian instance
    Pot::PolyType
    pot::Function

    # continuity class (0, 1 or 2)
    cclass::Int

    # derivatives of pot, if cclass >= 1
    DX::PolyType
    dx::Function
    DY::PolyType
    dy::Function

    # second derivatives of pot, if cclass >= 2
    DXX::PolyType
    dxx::Function
    DXY::PolyType
    dxy::Function
    DYY::PolyType
    dyy::Function

    # vector field vx and vy : R^2 → R
    VX::PolyType
    vx::Function
    VY::PolyType
    vy::Function

    # integrating factor σ : R^2 → R, that is:
    # σ * vx = - dy
    # σ * vy =   dx
    Σ::PolyType
    σ::Function

    # inner constructor
    function PolynomialHamiltonian(Pot::PolyType, VX::PolyType, VY::PolyType, Σ::PolyType)
        pot(x,y) = subs(Pot, X=>x, Y=>y)
        DX = TypedPolynomials.differentiate(Pot, X)
        dx(x,y) = subs(DX, X=>x, Y=>y)
        DY = TypedPolynomials.differentiate(Pot, Y)
        dy(x,y) = subs(DY, X=>x, Y=>y)
        DXX = TypedPolynomials.differentiate(DX, X)
        dxx(x,y) = subs(DXX, X=>x, Y=>y)
        DXY = TypedPolynomials.differentiate(DX, Y)
        dxy(x,y) = subs(DXY, X=>x, Y=>y)
        DYY = TypedPolynomials.differentiate(DY, Y)
        dyy(x,y) = subs(DYY, X=>x, Y=>y)
        vx(x,y) = subs(VX, X=>x, Y=>y)
        vy(x,y) = subs(VY, X=>x, Y=>y)
        σ(x,y) = subs(Σ, X=>x, Y=>y)
        return new(Pot, pot, 2, DX, dx, DY, dy, DXX, dxx, DXY, dxy, DYY, dyy, VX, vx, VY, vy, Σ, σ)
    end

end


mutable struct OvalFamily
    # center of the oval
    x0::Real
    y0::Real
    # transversal angle
    α::Real
    # min and max radius defining the portion of interest of the transversal
    rmin::Real
    rmax::Real
    # orientation: 1 = posive (counter-clockwise), -1 = negative (clockwise)
    orient::Int

    # inner constructor
    function OvalFamily(x0::Real, y0::Real, α::Real, rmin::Real, rmax::Real, orient::Int)
        if rmin < 0
            error("rmin must be nonnegative")
        elseif rmax < rmin
            error("rmax cannot be smaller than rmin")
        elseif orient != 1 && orient != -1
            error("orient must be -1 or 1")
        else
            new(x0, y0, α, rmin, rmax, orient)
        end
    end

end

# other constructors
OvalFamily(x0::Real, y0::Real, α::Real, rmin::Real, rmax::Real) = OvalFamily(x0, y0, α, rmin, rmax, 1)

OvalFamily(x0::T, y0::T, α::T, orient::Int) where {T<:Real} = OvalFamily(x0, y0, α, T(0), T(∞), orient)

OvalFamily(x0::T, y0::T, α::T) where {T<:Real} = OvalFamily(x0, y0, α, T(0), T(∞))

# convert radius on oval family into level h
function ovallevel(H::AbstractHamiltonian, ov::OvalFamily, r::T) where {T<:Real}
    x0 = T(ov.x0)
    y0 = T(ov.y0)
    α = T(ov.α)
    h = T(H(x0 + r * cos(α), y0 + r * sin(α)))
    return h
end

# type for perturbations

abstract type AbstractPerturbation end

# type for perturbations represented by general functions

mutable struct FunctionPerturbation <: AbstractPerturbation
    # number of perturbations
    dim::Int
    # functions R^2 → Vector{R} (of size dim) representing the x and y components of the perturbations
    wx::Function
    wy::Function
    # Green term corresponding to (σ*wx)'_x + (σ*wy)'_y where σ is the integration factor in the Hamiltonian
    green::Union{Function,Nothing}

    # inner constructor
    FunctionPerturbation(dim::Int, wx::Function, wy::Function, green=nothing::Union{Function, Nothing}) = new(dim, wx, wy, green)

end

# type for perturbations represented by polynomial/rational functions

mutable struct PolynomialPerturbation <: AbstractPerturbation
    # number of perturbations
    dim::Int
    # functions R^2 → Vector{R} (of size dim) representing the x and y components of the perturbations -- defined by polynomials
    WX::Vector{PolyType}
    wx::Function
    WY::Vector{PolyType}
    wy::Function
    # Green term corresponding to (σ*wx)'_x + (σ*wy)'_y where σ is the integration factor in the Hamiltonian
    Green::Union{Vector{PolyType}, Nothing}
    green::Union{Function,Nothing}

    # inner constructor
    function PolynomialPerturbation(dim::Int, WX::Vector{PolyType}, WY::Vector{PolyType}, Green=nothing::Union{Vector{PolyType}, Nothing})
        if length(WX) != dim || length(WY) != dim
            error("dim does not match length of array of polynomials WX or WY")
        else
            wx(x::T, y::T) where {T} = [WXi(X=>x, Y=>y) for WXi=WX]
            wy(x::T, y::T) where {T} = [WYi(X=>x, Y=>y) for WYi=WY]
            if Green != nothing
                if length(Green) != dim
                    error("dim does not match length of array of polynomials Green")
                end
                green(x::T, y::T) where {T} = [Gi(X=>x, Y=>y) for Gi=Green]
            else
                green = nothing
            end
            return new(dim, WX, wx, WY, wy, Green, green)
        end
    end

end

# outer constructor
# automatically construct the Green term used in the Abelian integral
function PolynomialPerturbation(H::PolynomialHamiltonian, dim::Int, WX::Vector{PolyType}, WY::Vector{PolyType})
    Green = [TypedPolynomials.differentiate(H.Σ * WX[i], X) + TypedPolynomials.differentiate(H.Σ * WY[i], Y) for i=1:dim]
    return PolynomialPerturbation(dim, WX, WY, Green)
end


# # outer constructor
#
# PolynomialPerturbation(WX::Vector{PolyType}, WY::Vector{PolyType}) = length(WX) != length(WY) ? error("arrays of polynomials WX and WY must have same length") : PolynomialPerturbation(length(WX), WX, WY)


end # module

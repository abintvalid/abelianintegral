
using IntervalArithmetic, ApproxFun, RigorousFourier, AbelianIntegral, Plots, Plots.PlotMeasures, LaTeXStrings, TypedPolynomials, LinearAlgebra

using AbelianIntegral: X, Y, PolyType
using IntervalArithmetic: Interval
using ApproxFun: (..)


RigorousFourier.set_default_len(256)
const T = BigFloat
RigorousFourier.set_default_RTA_fastmul(true)
RigorousFourier.set_default_usepow2(true)
setprecision(T, 256)
const IT = Interval{T}
T(x::IT) = mid(x)


## Numerical Wronskian
##

# if a function is assigned to h_to_r, then returns the Abelian integral as a function of h instead of r, using h_to_r to convert from h to r.
function abintapprox(::Type{T}, H, ov, pert, rmin, rmax, N; iterprec=1e-10, newtoniterations=10, maxdefect=1e-30, h_to_r=:none) where {T<:Real}
  if h_to_r == :none
    CS = Chebyshev(T(rmin)..T(rmax))
    rr = points(CS, N)
  else
    hrmin = H(ov.x0 + rmin * cos(ov.α), ov.y0 + rmin * sin(ov.α))
    hrmax = H(ov.x0 + rmax * cos(ov.α), ov.y0 + rmax * sin(ov.α))
    hmin = T(min(hrmin, hrmax))
    hmax = T(max(hrmin, hrmax))
    CS = Chebyshev(hmin..hmax)
    rr = h_to_r.(points(CS, N))
  end
  II = (r -> abelianintegral_trapez(T, H, ov, pert, r, iterprec=iterprec, newtoniterations=newtoniterations, maxdefect=maxdefect)).(rr)
  II = hcat(II...)
  FII = [Fun(CS, transform(CS, II[i,:])) for i=1:pert.dim]
  return FII
end

function wronskian(::Type{T}, FII, rmin, rmax, N) where {T<:Real}
  CS = Chebyshev(T(rmin)..T(rmax))
  l = length(FII)
  WW = fill(FII[1], (l,l))
  WW[1,:] = FII
  for i=2:l
    WW[i,:] = (f -> f').(WW[i-1,:])
  end
  WWW = (f -> resize!(itransform(CS, [coefficient(f, i) for i=1:N]), N)).(WW)
  Wpts = [det((v -> v[i]).(WWW)) for i=1:N]
  W = Fun(CS, transform(CS, Wpts))
  return W
end



## Dumortier Li intermediate system (figure-eight loop)
##

# Dumortier-Li System
function DL_system(λ)
  FX = X^4 / 4 + (1 - λ) * X^3 / 3 - λ * X^2 / 2
  DLpot = FX + Y^2
  DLVX = - TypedPolynomials.differentiate(DLpot, Y)
  DLVY =   TypedPolynomials.differentiate(DLpot, X)
  DLΣ = IT(1) + 0*X
  H = PolynomialHamiltonian(DLpot, DLVX, DLVY, DLΣ)
  return H
end

# ovals of Dumortier-Li system
function DL_ovals(H, λ)
  # left
  ov1 = OvalFamily(IT(-1), IT(0), IT(0), IT(0), IT(1), 1)
  # right
  ov2 = OvalFamily(λ, IT(0), IT(π), IT(0), λ, 1)
  #external
  ov3 = OvalFamily(IT(0), IT(0), IT(π)/2, IT(0), IT(∞), 1)
  return ov1, ov2, ov3
end

# monomial perturbations for Dumorier-Li
function DL_pert(H)
  pertx = convert(Vector{PolyType}, fill(IT(0)*X, 4))
  perty = convert(Vector{PolyType}, [Y, Y*X, Y*X^2, Y^3])
  pert = PolynomialPerturbation(H, 4, pertx, perty)
  return pert
end


# plot Dumortier-Li figure 8 loop

setprecision(T, 256)
λ = T(7)/T(10)
H = DL_system(λ)
ov1, ov2, ov3 = DL_ovals(H, λ)
pert = DL_pert(H)
# special points
P1 = (λ, T(0))
P2 = (T(0), T(0))
P3 = (T(-1), T(0))

r1 = ov1.rmax*3/4
tend1, x1, y1, _, _ = ovalapprox_iterative(T, H, ov1, r1, prec=1e-20)
t1 = LinRange(T(0), tend1, 5000)
xt1 = x1.(t1)
yt1 = y1.(t1)

r2 = ov2.rmax*3/4
tend2, x2, y2, _, _ = ovalapprox_iterative(T, H, ov2, r2, prec=1e-20)
t2 = LinRange(T(0), tend2, 5000)
xt2 = x2.(t2)
yt2 = y2.(t2)

r3 = T(1//3)
tend3, x3, y3, _, _ = ovalapprox_iterative(T, H, ov3, r3, prec=1e-20)
t3 = LinRange(T(0), tend3, 5000)
xt3 = x3.(t3)
yt3 = y3.(t3)

r10 = T(0.0001)
tend10, x10, y10, _, _ = ovalapprox_iterative(T, H, ov3, r10, prec=1e-20)
t10 = LinRange(T(0), tend10, 5000)
xt10 = x10.(t10)
yt10 = y10.(t10)


plot(legend=:none,
     framestyle=:box,
     aspect_ratio=1.5,
     xlims=(-1.7,1.4),
     ylims=(-0.7,0.7),
     xticks=([-1,0,λ,1], [L"-1", L"0", L"\lambda", L"1"]),
     yticks=([-0.5,0,0.5], [L"-0.5", L"0", L"0.5"]),
     tickfontsize=12,
     windowsize=(600,400),
     xlabel=L"x",
     ylabel=L"\widetilde y",
     )
plot!(twinx(),
     framestyle=:box,
     aspect_ratio=1.5,
     xlims=(-1.7,1.4),
     ylims=(-0.7,0.7),
     xticks=([-1, 0, λ, 1], ["", "", "", ""]),
     yticks=([-0.5, 0, 0.5], ["", "", ""]),
     tickfontsize=12,
     grid=true,
     )
plot!(twiny(),
     framestyle=:box,
     aspect_ratio=1.5,
     xlims=(-1.7,1.4),
     ylims=(-0.7,0.7),
     xticks=([-1, 0, λ, 1], ["", "", "", ""]),
     yticks=([-0.5, 0, 0.5], ["", "", ""]),
     tickfontsize=12,
     grid=true,
     )
plot!([-1.7,1.4], [0,0],
     lw=1,
     lc=:black,
)
plot!([0,0], [-0.7,0.7],
     lw=1,
     lc=:black,
)

plot!(xt1, yt1,
  lc=:purple,
  lw=2)
plot!(xt2, yt2,
  lc=:dodgerblue3,
  lw=2)
plot!(xt3, yt3,
  lc=:forestgreen,
  lw=2)
plot!(xt10, yt10,
  lc=:gray,
  ls=:dash)

scatter!([P1[1]], [P1[2]],
  markercolor=:black,
  markersize=3)
annotate!([P1[1]], [P1[2]-0.05], text(L"\ \,\widetilde P_1", :left, 10))

scatter!([P2[1]], [P2[2]],
  markercolor=:black,
  markersize=3)
annotate!([P2[1]-0.07], [P2[2]+0.05], text(L"\widetilde P_2", :bottom, 10))

scatter!([P3[1]], [P3[2]],
  markercolor=:black,
  markersize=3)
annotate!([P3[1]], [P3[2]-0.05], text(L"\widetilde P_3\ ", :right, 10))

annotate!(0.4, 0.07, text(L"\widetilde\Gamma_1", 10, :purple))
annotate!(-0.4, 0.07, text(L"\widetilde\Gamma_2", 10, :dodgerblue3))
annotate!(0.1, -0.28, text(L"\widetilde\Gamma_3", 10, :forestgreen))

savefig("dumortierli-ovals.pdf")

# this value for lambda was found to get 2 zeros in the wronskian
λ = IT(17)/IT(100)
H = DL_system(λ)
ov1, ov2, ov3 = DL_ovals(H, λ)
pert = DL_pert(H)

# determine coefficients β of the perturbation using the Wronskian, to get 5 zeros on oval 3
β = fill(T(0), 4)
rmin3 = 0.001
rmax3 = 0.1
I3 = abintapprox(T, H, ov3, pert, rmin3, rmax3, 200)
W3 = wronskian(T, I3, rmin3, rmax3, 200)
rW3 = roots(W3)
plot(W3)
scatter!(rW3, _->0)
pts3 = fill(T(0), 3)
pts3[1] = rW3[1]
pts3[2] = (rW3[1] + rW3[2]) / 2
pts3[3] = rW3[2]
β[1] = T(1)
β[2:4] = - [I3[j](pts3[i]) for i=1:3, j=2:4] \ [I3[1](pts3[i]) for i=1:3]
β
I3β = sum(β .* I3)
rI3β = roots(I3β)
plot(I3β)
scatter!(rI3β, _->0)

# check 1 zero on oval 1
rmin1 = 0.8
rmax1 = 0.99
I1 = abintapprox(T, H, ov1, pert, rmin1, rmax1, 200)
I1β = sum(β .* I1)
rI1β = roots(I1β)
plot(I1β)
scatter!(rI1β, _->0)

# plot Wronskian for this λ and the limit value λ0
# (double root for the wronskian, as a function of h)
λ0 = IT(0.185)
H0 = DL_system(λ0)
ov01, ov02, ov03 = DL_ovals(H0, λ0)
pert0 = DL_pert(H0)
rmin03 = 0.001
rmax03 = 0.1
I03 = abintapprox(T, H0, ov03, pert0, rmin03, rmax03, 200)
W03 = wronskian(T, I03, rmin03, rmax03, 200)
drW03 = roots(W03')[3]
rpts = LinRange(0.001, 0.095, 200)

plot(rpts, W3.(rpts),
     framestyle=:box,
     legend=:bottomleft,
     right_margin=25mm,
     # aspect_ratio=0.01,
     xlims=(0,0.1),
     ylims=(-3,3),
     xlabel=L"\widetilde r = \sqrt{\widetilde h}",
     ylabel=L"\widetilde W_3",
     xticks=([0,0.02,0.04,0.06,0.08,0.1], [L"0", L"0.02", L"0.04", L"0.06", L"0.08", L"0.1"]),
     yticks=([-3,-2,-1,0,1,2,3], [L"-3", L"-2", L"-1", L"0", L"1", L"2", L"3"]),
     tickfontsize=10,
     windowsize=(550,400),
     lc=:orangered,
     lw=2,
     label=L"\widetilde W_3 \quad (\lambda=0.17)",
     )
plot!(rpts, W03.(rpts),
     lc=:orangered,
     lw=2,
     ls=:dash,
     label=L"\widetilde W_3 \quad (\lambda=\lambda^*)",
     )
plot!(twinx(), rpts, I3β.(rpts),
     framestyle=:axes,
     xmirror=true,
     xlims=(0,0.1),
     ylims=(-0.0006,0.0006),
     tickfontsize=10,
     xticks=([0,0.02,0.04,0.06,0.08,0.1], ["", "", "", "", "", ""]),
     yticks=([-0.0006, -0.0004, -0.0002, 0, 0.0002, 0.0004, 0.0006], [L"-0.0006", L"-0.0004", L"-0.0002", L"0", L"0.0002", L"0.0004", L"0.0006"]),
     #grid=:none,
     legend=:bottomright,
     ylabel=L"\widetilde \mathfrak{I}_3",
     lc=:dodgerblue3,
     lw=2,
     label=L"\widetilde \mathfrak{I}_3 \quad (\lambda=0.17)",
     )
plot!([0, 0.1], _->0,
     lw=1,
     lc=:black,
     label="",
     )
scatter!(twinx(), vcat(rI3β, [drW03]), _->0,
    framestyle=:none,
    legend=:none,
    xlims=(0,0.1),
    ylims=(-0.0006,0.0006),
    markersize=4,
    markerstrokewidth=1,
    markerstrokecolor=[:dodgerblue3,:orangered,:dodgerblue3,:orangered,:dodgerblue3,:orangered],
    markercolor=:white)
annotate!(rI3β[1]+0.002, 0-0.1,
    text(L"\widetilde r_4", 12, :dodgerblue3, :top)
    )
annotate!(rI3β[2], 0-0.1,
    text(L"\widetilde r_1", 12, :orangered, :top)
    )
annotate!(rI3β[3]-0.001, 0-0.1,
    text(L"\widetilde r_3", 12, :dodgerblue3, :top)
    )
annotate!(drW03+0.001, 0-0.1,
    text(L"\widetilde r_0", 12, :orangered, :top)
    )
annotate!(rI3β[4]+0.001, 0-0.1,
    text(L"\widetilde r_2", 12, :orangered, :top)
    )
annotate!(rI3β[5]+0.003, 0-0.1,
    text(L"\widetilde r_5", 12, :dodgerblue3, :top)
    )
plot!(legendfontsize=10)
plot!(twiny(),
  xticks=:none)
savefig("dumortierli-wronskian.pdf")


## Li, Liu and Yang's cubic system with 13 limit cycles
##

function LLY_system(λ, κ)
  FX = X^4 / 4 + (1 - λ) * X^3 / 3 - λ * X^2 / 2
  LLYpot = FX + Y^4 / 4 - κ^2 * Y^2 / 2
  LLYVX = - TypedPolynomials.differentiate(LLYpot, Y)
  LLYVY =   TypedPolynomials.differentiate(LLYpot, X)
  LLYΣ = IT(1) + 0*X
  H = PolynomialHamiltonian(LLYpot, LLYVX, LLYVY, LLYΣ)
  return H
end

function LLY_pert(H)
  pertx = convert(Vector{PolyType}, fill(IT(0)*X, 4))
  perty = convert(Vector{PolyType}, [Y, Y*X, Y*X^2, Y^3])
  pert = PolynomialPerturbation(H, 4, pertx, perty)
  return pert
end

function LLY_ovals(λ, κ)
  # oval 1 = small, upper right
  ov1 = OvalFamily(λ, κ, IT(π), IT(0), λ, 1)
  # oval 2 = small, upper left
  ov2 = OvalFamily(IT(-1), κ, IT(0), IT(0), IT(1), 1)
  # oval 3 = upper medium
  ov3 = OvalFamily(IT(0), κ, IT(π)/2, IT(0), sqrt(κ^2 + 2 * sqrt(-(2 * λ + 1) / 12 + κ^4 / 4)) - κ, 1)
  # oval 4 = small, lower right
  ov4 = OvalFamily(λ, -κ, IT(π), IT(0), λ, 1)
  # oval 5 = small, lower left
  ov5 = OvalFamily(IT(-1), -κ, IT(0), IT(0), IT(1), 1)
  # oval 6 = lower medium
  ov6 = OvalFamily(IT(0), -κ, -IT(π)/2, IT(0), sqrt(κ^2 + 2 * sqrt(-(2 * λ + 1) / 12 + κ^4 / 4)) - κ, 1)
  # oval 7 = outer
  ov7 = OvalFamily(IT(0), IT(0), IT(0), λ, IT(∞), 1)
  # oval 8 = large
  ov8 = OvalFamily(IT(0), κ, IT(π)/2, sqrt(κ^2 + 2 * sqrt(-(2 * λ + 1) / 12 + κ^4 / 4)) - κ, sqrt(κ^2 + 2 * sqrt(-λ^3 * (λ + 2) / 12 + κ^4 / 4)) - κ, 1)
  # oval 9 = small in the middle
  ov9 = OvalFamily(IT(0), IT(0), IT(0), IT(0), λ, 1)
  return ov1, ov2, ov3, ov4, ov5, ov6, ov7, ov8, ov9
end


# plot Li-Liu-Yang system

setprecision(T, 256)
λ = T(7)/T(10)
κ = T(11)/T(10)
H = LLY_system(λ, κ)
pert = LLY_pert(H)
ov1, ov2, ov3, ov4, ov5, ov6, ov7, ov8, ov9 = LLY_ovals(λ, κ)

P1 = (λ, κ)
P2 = (T(0), κ)
P3 = (T(-1), κ)
P4 = (λ, T(0))
P5 = (T(0), T(0))
P6 = (T(-1), T(0))
P7 = (λ, -κ)
P8 = (T(0), -κ)
P9 = (T(-1), -κ)

r1 = ov1.rmax*3/4
tend1, x1, y1, _, _ = ovalapprox_iterative(T, H, ov1, r1, prec=1e-20)
tt1 = LinRange(T(0), tend1, 5000)
xt1 = x1.(tt1)
yt1 = y1.(tt1)

r2 = ov2.rmax*3/4
tend2, x2, y2, _, _ = ovalapprox_iterative(T, H, ov2, r2, prec=1e-20)
tt2 = LinRange(T(0), tend2, 5000)
xt2 = x2.(tt2)
yt2 = y2.(tt2)

r3 = ov3.rmax*9/10
tend3, x3, y3, _, _ = ovalapprox_iterative(T, H, ov3, r3, prec=1e-20)
tt3 = LinRange(T(0), tend3, 5000)
xt3 = x3.(tt3)
yt3 = y3.(tt3)

r4 = ov4.rmax*3/4
tend4, x4, y4, _, _ = ovalapprox_iterative(T, H, ov4, r4, prec=1e-20)
tt4 = LinRange(T(0), tend4, 5000)
xt4 = x4.(tt4)
yt4 = y4.(tt4)

r5 = ov5.rmax*3/4
tend5, x5, y5, _, _ = ovalapprox_iterative(T, H, ov5, r5, prec=1e-20)
tt5 = LinRange(T(0), tend5, 5000)
xt5 = x5.(tt5)
yt5 = y5.(tt5)

r6 = ov6.rmax*9/10
tend6, x6, y6, _, _ = ovalapprox_iterative(T, H, ov6, r6, prec=1e-20)
tt6 = LinRange(T(0), tend6, 5000)
xt6 = x6.(tt6)
yt6 = y6.(tt6)

r7 = ov7.rmin*1.5
tend7, x7, y7, _, _ = ovalapprox_iterative(T, H, ov7, r7, prec=1e-20)
tt7 = LinRange(T(0), tend7, 5000)
xt7 = x7.(tt7)
yt7 = y7.(tt7)

r8 = (ov8.rmin+ov8.rmax)/2
tend8, x8, y8, _, _ = ovalapprox_iterative(T, H, ov8, r8, prec=1e-20)
tt8 = LinRange(T(0), tend8, 5000)
xt8 = x8.(tt8)
yt8 = y8.(tt8)

r9 = ov9.rmax/2
tend9, x9, y9, _, _ = ovalapprox_iterative(T, H, ov9, r9, prec=1e-20)
tt9 = LinRange(T(0), tend9, 5000)
xt9 = x9.(tt9)
yt9 = y9.(tt9)

r10 = T(0.0001)
tend10, x10, y10, _, _ = ovalapprox_iterative(T, H, ov3, r10, prec=1e-20)
tt10 = LinRange(T(0), tend10, 5000)
xt10 = x10.(tt10)
yt10 = y10.(tt10)

r11 = ov3.rmax*999/1000
tend11, x11, y11, _, _ = ovalapprox_iterative(T, H, ov3, r11, prec=1e-20)
tt11 = LinRange(T(0), tend11, 5000)
xt11 = x11.(tt11)
yt11 = y11.(tt11)

r12 = T(0.0001)
tend12, x12, y12, _, _ = ovalapprox_iterative(T, H, ov6, r12, prec=1e-20)
tt12 = LinRange(T(0), tend12, 5000)
xt12 = x12.(tt12)
yt12 = y12.(tt12)

r13 = ov6.rmax*999/1000
tend13, x13, y13, _, _ = ovalapprox_iterative(T, H, ov6, r13, prec=1e-20)
tt13 = LinRange(T(0), tend13, 5000)
xt13 = x13.(tt13)
yt13 = y13.(tt13)

r14 = ov8.rmax*9999/10000
tend14, x14, y14, _, _ = ovalapprox_iterative(T, H, ov8, r14, prec=1e-20)
tt14 = LinRange(T(0), tend14, 5000)
xt14 = x14.(tt14)
yt14 = y14.(tt14)

plot(legend=:none,
     aspect_ratio=1,
     xlims=(-2,2),
     ylims=(-2,2),
     xticks=([-2,-1,0,λ,1,2], [L"-2", L"-1", L"0", L"\lambda", L"1", L"2"]),
     yticks=([-2,-1,0,1,κ,2], [L"-2", L"-1", L"0", L"1", "", L"2"]),
     tickfontsize=12,
     windowsize=(500,500),
     grid=:none,
     xlabel=L"x",
     ylabel=L"y",
     )
plot!(twinx(), xmirror=true,
     aspect_ratio=1,
     xlims=(-2,2),
     ylims=(-2,2),
     xticks=([-2, -1, 0, λ, 1, 2], ["", "", "", "", "", "", ""]),
     yticks=([-2, -κ, -1, 0, 1, κ, 2], ["", L"-\kappa", "", "", "", L"\kappa", ""]),
     tickfontsize=12,
     grid=:none,
     )
plot!(twinx(),
     aspect_ratio=1,
     framestyle=:grid,
     xlims=(-2,2),
     ylims=(-2,2),
     xticks=([-2, -1, 0, 1, 2], ["", "", "", "", ""]),
     yticks=([-2, -1, 0, 1, 2], ["", "", "", "", ""]),
     )
plot!(twinx(),
     aspect_ratio=1,
     framestyle=:grid,
     xlims=(-2,2),
     ylims=(-2,2),
     xticks=([λ], [""]),
     yticks=([-κ, κ], ["", ""]),
     gridstyle=:dash,
     )
plot!([-2,2], [0,0],
     lw=1,
     lc=:black,
)
plot!([0,0], [-2,2],
     lw=1,
     lc=:black,
)

plot!(xt1, yt1,
  lc=:purple,
  lw=2)
plot!(xt2, yt2,
  lc=:dodgerblue3,
  lw=2)
plot!(xt3, yt3,
  lc=:forestgreen,
  lw=2)
plot!(xt4, yt4,
  lc=:purple,
  lw=2)
plot!(xt5, yt5,
  lc=:dodgerblue3,
  lw=2)
plot!(xt6, yt6,
  lc=:forestgreen,
  lw=2)
plot!(xt7, yt7,
  lc=:firebrick2,
  lw=2)
plot!(xt8, yt8,
  lc=:orange,
  lw=2)
plot!(xt9, yt9,
  lc=:hotpink,
  lw=2)
plot!(xt10, yt10,
  lc=:gray,
  ls=:dash)
plot!(xt11, yt11,
  lc=:gray,
  ls=:dash)
plot!(xt12, yt12,
  lc=:gray,
  ls=:dash)
plot!(xt13, yt13,
  lc=:gray,
  ls=:dash)
plot!(xt14, yt14,
  lc=:gray,
  ls=:dash)

scatter!([P1[1]], [P1[2]],
  markercolor=:black,
  markersize=3)
annotate!([P1[1]], [P1[2]], text(L"\ \,P_1", :left, 10))

scatter!([P2[1]], [P2[2]],
  markercolor=:black,
  markersize=3)
annotate!([P2[1]-0.1], [P2[2]+0.05], text(L"P_2", :bottom, 10))

scatter!([P3[1]], [P3[2]],
  markercolor=:black,
  markersize=3)
annotate!([P3[1]], [P3[2]], text(L"P_3\ ", :right, 10))

scatter!([P4[1]], [P4[2]],
  markercolor=:black,
  markersize=3)
annotate!([P4[1]], [P4[2]-0.1], text(L"P_4\ \ ", :right, 10))

scatter!([P5[1]], [P5[2]],
  markercolor=:black,
  markersize=3)
annotate!([P5[1]], [P5[2]-0.1], text(L"P_5\ ", :right, 10))

scatter!([P6[1]], [P6[2]],
  markercolor=:black,
  markersize=3)
annotate!([P6[1]], [P6[2]-0.1], text(L"\ \ P_6", :left, 10))

scatter!([P7[1]], [P7[2]],
  markercolor=:black,
  markersize=3)
annotate!([P7[1]], [P7[2]], text(L"\ \,P_7", :left, 10))

scatter!([P8[1]], [P8[2]],
  markercolor=:black,
  markersize=3)
annotate!([P8[1]-0.1], [P8[2]-0.05], text(L"P_8", :top, 10))

scatter!([P9[1]], [P9[2]],
  markercolor=:black,
  markersize=3)
annotate!([P9[1]], [P9[2]], text(L"P_9\ ", :right, 10))

annotate!(0.4, κ, text(L"\Gamma_1", 10, :purple))
annotate!(-0.4, κ, text(L"\Gamma_2", 10, :dodgerblue3))
annotate!(0.1, 0.8, text(L"\Gamma_3", 10, :forestgreen))
annotate!(0.4, -κ, text(L"\Gamma_4", 10, :purple))
annotate!(-0.4, -κ, text(L"\Gamma_5", 10, :dodgerblue3))
annotate!(0.1, -0.8, text(L"\Gamma_6", 10, :forestgreen))
annotate!(1.2, 0.1, text(L"\Gamma_7", 10, :firebrick2))
annotate!(λ, 0.2, text(L"\Gamma_8", 10, :orange))
annotate!(0.15, 0.1, text(L"\Gamma_9", 10, :hotpink))

savefig("liliuyang-ovals.pdf")



# computer assisted proofs of 13 limit cycles

setprecision(T, 256)
λ = IT(17)/IT(100)
κ = IT(20) # choose κ sufficiently large
H = LLY_system(λ, κ)
pert = LLY_pert(H)
ov1, ov2, ov3, ov4, ov5, ov6, ov7, ov8, ov9 = LLY_ovals(λ, κ)

# coefficients α deduced from coefficients β of Dumortier-Li
# according to Li-Liu-Yang's article
α = fill(T(0), 4)
α[1] = β[1] / κ^5 + (3 * β[4]) / (2 * κ)
α[2] = β[2] / κ^5
α[3] = β[3] / κ^5
α[4] = - β[4] / (2 * κ^3)

# check 5 zeros on oval 3
rmin3 = T(0.0001)
rmax3 = T(0.0045)
I3 = abintapprox(T, H, ov3, pert, rmin3, rmax3, 200)
I3α = sum(α .* I3)
rI3α = roots(I3α)
plot(I3α)
scatter!(rI3α, _->0)

# round coefficients to 12 digits

# round to d digits in scientific notation
# returns BigInts y and M, representing y/M
function round_coeff(x::T, d)
  if x == 0
    return BigInt(0),BigInt(1)
  else
    i = -floor(log10(abs(x)))
    M = BigInt(10)^(i+d)
    y = BigInt(round(x*M))
    return y,M
  end
end

αnum = fill(BigInt(0), 4)
αden = fill(BigInt(0), 4)
αα = fill(IT(0), 4)
for i=1:4
  αnum[i], αden[i] = round_coeff(α[i], 12)
  αα[i] = IT(αnum[i]) / IT(αden[i])
end
α = T.(αα)


# check 1 zero on oval 2
rmin2 = T(0.85)
rmax2 = T(0.95)
I2 = abintapprox(T, H, ov2, pert, rmin2, rmax2, 200)
I2α = sum(α .* I2)
rI2α = roots(I2α)
plot(I2α)
scatter!(rI2α, _->0)

# check 1 zero on oval 7
rmin7 = T(10)
rmax7 = T(30)
I7 = abintapprox(T, H, ov7, pert, rmin7, rmax7, 200)
I7α = sum(α .* I7)
rI7α = roots(I7α)
plot(I7α)
scatter!(rI7α, _->0)


# plot these experiments for the paper
r2pts = LinRange(rmin2, rmax2, 200)
plot(I2α)

pI2 = plot(r2pts, I2α.(r2pts),
     legend=:bottomright,
     # right_margin=25mm,
     # aspect_ratio=0.01,
     xlims=(0.85, 0.93),
     ylims=(-2.5e-9,1e-9),
     framestyle=:box,
     xlabel=L"r \quad s.t. \quad H(-1+r,\kappa)=h",
     ylabel="",
     xticks=([0.86,0.88,0.9,0.92], [L"0.86", L"0.88", L"0.90", L"0.92"]),
     yticks=([-2e-9,-1e-9,0,1e-9], [L"-2 \times 10^{-9}", L"-1 \times 10^{-9}", L"0", L"1 \times 10^{-9}"]),
     # windowsize=(500,400),
     lc=:dodgerblue3,
     lw=2,
     label=L"\mathfrak{I}_2(h)",
     )
plot!([0.85, 0.93], [0, 0],
    lw=1,
    lc=:black,
    label="",
)
scatter!(rI2α, _->0,
    markersize=4,
    markerstrokewidth=1,
    markercolor=:white,
    markerstrokecolor=:dodgerblue3,
    label="",
)
plot(pI2, windowsize=(400,300))
savefig("liliuyang-I2.pdf")

r3pts = LinRange(rmin3, rmax3, 200)
plot(I3α)

pI3 = plot(r3pts, I3α.(r3pts),
     legend=:bottomleft,
     # right_margin=25mm,
     # aspect_ratio=0.01,
     xlims=(0, 0.0045),
     ylims=(-7e-12,2.5e-12),
     framestyle=:box,
     xlabel=L"r \quad s.t. \quad H(0,\kappa+r)=h",
     ylabel="",
     xticks=([0,1e-3,2e-3,3e-3,4e-3], [L"0", L"0.001", L"0.002", L"0.003", L"0.004"]),
     yticks=([-6e-12,-4e-12,-2e-12,0,2e-12], [L"-6 \times 10^{-12}", L"-4 \times 10^{-12}", L"-2 \times 10^{-12}", L"0", L"2 \times 10^{-12}"]),
     # windowsize=(500,400),
     lc=:forestgreen,
     lw=2,
     label=L"\mathfrak{I}_3(h)",
     )
plot!([0, 0.0045], [0, 0],
    lw=1,
    lc=:black,
    label="",
)
scatter!(rI3α, _->0,
    markersize=4,
    markerstrokewidth=1,
    markercolor=:white,
    markerstrokecolor=:forestgreen,
    label="",
)
plot(pI3, windowsize=(400,300))
savefig("liliuyang-I3.pdf")

r7pts = LinRange(rmin7, rmax7, 200)
plot(I7α)

pI7 = plot(r7pts, I7α.(r7pts),
     legend=:bottomleft,
     # right_margin=25mm,
     # aspect_ratio=0.01,
     xlims=(10, 30),
     ylims=(-3000,5500),
     framestyle=:box,
     xlabel=L"r \quad s.t. \quad H(r,0)=h",
     ylabel="",
     xticks=([10,15,20,25,30], [L"10", L"15", L"20", L"25", L"30"]),
     yticks=([-2000,0,2000,4000], [L"-2000", L"0", L"2000", L"4000"]),
     # windowsize=(500,400),
     lc=:firebrick2,
     lw=2,
     label=L"\mathfrak{I}_7(h)",
     )
plot!([10, 30], [0, 0],
    lw=1,
    lc=:black,
    label="",
)
scatter!(rI7α, _->0,
    markersize=4,
    markerstrokewidth=1,
    markercolor=:white,
    markerstrokecolor=:firebrick2,
    label="",
)
plot(pI7, windowsize=(400,300))
savefig("liliuyang-I7.pdf")

plot(pI2, pI3, pI7,
    layout=(1,3),
    windowsize=(1200,300),
    bottom_margin=10mm
)
plot!(tickfontsize=12,
    legendfontsize=12,
    guidefontsize=15)
savefig("liliuyang-I237.pdf")


## validated enclosures for the 13 limit cycles

pertαx = convert(Vector{PolyType}, [IT(0)*X])
pertαy = convert(Vector{PolyType}, [αα' * [Y, Y*X, Y*X^2, Y^3]])
pertα = PolynomialPerturbation(H, 1, pertαx, pertαy)

function fpLLY()
  FXf = X^4 / T(4) + (1 - T(λ)) * X^3 / T(3) - T(λ) * X^2 / T(2)
  LLYpotf = FXf + Y^4 / T(4) - T(κ)^2 * Y^2 / T(2)
  LLYVXf = - TypedPolynomials.differentiate(LLYpotf, Y)
  LLYVYf =   TypedPolynomials.differentiate(LLYpotf, X)
  LLYΣf = T(1) + T(0)*X
  Hf = PolynomialHamiltonian(LLYpotf, LLYVXf, LLYVYf, LLYΣf)
  return Hf
end
setprecision(T, 512)
Hf = fpLLY()
# evaluation points
rr2 = [IT(88)/100, IT(92)/100]
rr3 = [IT(2)/10000, IT(10)/10000, IT(15)/10000, IT(22)/10000, IT(40)/10000, IT(45)/10000]
#rr3 = IT.([0.0002, 0.001, 0.0015, 0.0022, 0.004, 0.0045])
rr7 = [IT(25), IT(30)]

RigorousFourier.set_default_RTA_fastmul(true)
#RigorousFourier.set_default_RTA_fastmul_interval(true)
RigorousFourier.set_default_RTA_fastmul_interval(false)
RigorousFourier.set_default_usepow2(true)

# oval 3
I3ev = fill(IT(0), 6)
RigorousFourier.set_default_len(8000)
@time I3ev[1] = abelianintegral_valid(T, Hf, ov3, pertα, rr3[1], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I3ev[1]))
RigorousFourier.set_default_len(8000)
@time I3ev[2] = abelianintegral_valid(T, Hf, ov3, pertα, rr3[2], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I3ev[2]))
RigorousFourier.set_default_len(4000)
@time I3ev[3] = abelianintegral_valid(T, Hf, ov3, pertα, rr3[3], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I3ev[3]))
RigorousFourier.set_default_len(4000)
@time I3ev[4] = abelianintegral_valid(T, Hf, ov3, pertα, rr3[4], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I3ev[4]))
RigorousFourier.set_default_len(4000)
@time I3ev[5] = abelianintegral_valid(T, Hf, ov3, pertα, rr3[5], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I3ev[5]))
RigorousFourier.set_default_len(2000)
@time I3ev[6] = abelianintegral_valid(T, Hf, ov3, pertα, rr3[6], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I3ev[6]))

# oval 2
I2ev = fill(IT(0), 2)
RigorousFourier.set_default_len(2000)
@time I2ev[1] = abelianintegral_valid(T, Hf, ov2, pertα, rr2[1], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I2ev[1]))
RigorousFourier.set_default_len(2000)
@time I2ev[2] = abelianintegral_valid(T, Hf, ov2, pertα, rr2[2], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I2ev[2]))

# oval 7
I7ev = fill(IT(0), 2)
RigorousFourier.set_default_len(100)
@time I7ev[1] = abelianintegral_valid(T, Hf, ov7, pertα, rr7[1], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I7ev[1]))
RigorousFourier.set_default_len(100)
@time I7ev[2] = abelianintegral_valid(T, Hf, ov7, pertα, rr7[2], iterprec=1e-20, newtoniterations=10, maxdefect=1e-50, denoising=1)[1]
Float64(diam(I7ev[2]))


## Timings

λ = IT(7)/IT(10)
κ = IT(11)/IT(10)
H = LLY_system(λ, κ)
pert = LLY_pert(H)
ov1, ov2, ov3, ov4, ov5, ov6, ov7, ov8, ov9 = LLY_ovals(λ, κ)
α = [IT(1), IT(1), IT(1), IT(1)]
pertαx = convert(Vector{PolyType}, [IT(0)*X])
pertαy = convert(Vector{PolyType}, [α' * [Y, Y*X, Y*X^2, Y^3]])
pertα = PolynomialPerturbation(H, 1, pertαx, pertαy)

RigorousFourier.set_default_RTA_fastmul(true)
RigorousFourier.set_default_RTA_fastmul_interval(false)

## oval 3

II = IT(0)

# target relative error = 1e-4
setprecision(T, 53)
RigorousFourier.set_default_len(100)
@time for i=1:10
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-10, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-6
setprecision(T, 53)
RigorousFourier.set_default_len(125)
@time for i=1:10
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-10, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-8
setprecision(T, 128)
RigorousFourier.set_default_len(170)
@time for i=1:10
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-10, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-16
setprecision(T, 128)
RigorousFourier.set_default_len(400)
@time for i=1:10
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-20, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-24
setprecision(T, 128)
RigorousFourier.set_default_len(625)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-30, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-32
setprecision(T, 256)
RigorousFourier.set_default_len(855)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-48
setprecision(T, 256)
RigorousFourier.set_default_len(1310)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-50, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-64
setprecision(T, 256)
RigorousFourier.set_default_len(1765)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-70, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-96
setprecision(T, 512)
RigorousFourier.set_default_len(2675)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-100, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-128
setprecision(T, 512)
RigorousFourier.set_default_len(3580)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov3, pertα, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-140, denoising=1.)[1]
end
abs((II-II) / (2*II))


## oval 8

# target relative error = 1e-4
setprecision(T, 53)
RigorousFourier.set_default_len(220)
@time for i=1:10
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-10, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-6
setprecision(T, 128)
RigorousFourier.set_default_len(280)
@time for i=1:10
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-10, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-8
setprecision(T, 128)
RigorousFourier.set_default_len(385)
@time for i=1:10
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-10, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-16
setprecision(T, 128)
RigorousFourier.set_default_len(860)
@time for i=1:10
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-20, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-24
setprecision(T, 128)
RigorousFourier.set_default_len(1330)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-30, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-32
setprecision(T, 256)
RigorousFourier.set_default_len(1805)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-48
setprecision(T, 256)
RigorousFourier.set_default_len(2750)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-50, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-64
setprecision(T, 512)
RigorousFourier.set_default_len(3695)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-70, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-96
setprecision(T, 512)
RigorousFourier.set_default_len(5585)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-100, denoising=1.)[1]
end
abs((II-II) / (2*II))

# target relative error = 1e-128
setprecision(T, 512)
RigorousFourier.set_default_len(7475)
@time for i=1:1
  II = abelianintegral_valid(T, H, ov8, pertα, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-140, denoising=1.)[1]
end
abs((II-II) / (2*II))

#include <iostream>
#include <chrono>
#include "capd/mpcapdlib.h"
using namespace capd;
using namespace std;
using namespace std::chrono;

// return (x,y,I) with (x,y) the first return and I the value of the Abelian integral
void computeAbelianIntegral(MpIVector u0, MpIVector r,
                   MpINonlinearSection section3D, int order, int BITS)
{
  // adjust floating-point precision
	MpFloat::setDefaultPrecision(BITS);
	cout << "Computing with " << BITS << " bits precision." << endl;
	cout.precision(10);

	// This is vector field for the quartic system
  MpIMap vectorField("par:X0,Y0,a00,a20,a22,a40,a04;var:x,y,I;fun:-4*y^2*(y^2 - Y0),4*y*x*(x^2 - X0),(a00+a20*x^2+a22*x^2*y^2+a40*x^4+a04*y^4)*4*y^2*(y^2-Y0)/y;");

  // set parameter values
  vectorField.setParameter("Y0", MpInterval(11.)/10.); // Y0=1.1
  vectorField.setParameter("X0", MpInterval(9.)/10.);  // X0=0.9
  vectorField.setParameter("a00", MpInterval(-78622148667854837664.)/100000000000000000000.);
  vectorField.setParameter("a20", MpInterval(87723523612653436051.)/100000000000000000000.);
  vectorField.setParameter("a22", MpInterval(1.));
  vectorField.setParameter("a40", MpInterval(23742713894293038223.)/100000000000000000000.);
  vectorField.setParameter("a04", MpInterval(-21823846173078863753.)/100000000000000000000.);

  // the solver uses high order enclosure method to verify the existence
  // of the solution.
  cout << "Integrating with order " << order << endl;
  MpIOdeSolver solver(vectorField, order);

  // Set up the Poincaré map.
  MpIPoincareMap pm(solver, section3D, poincare::PlusMinus);

  // Compute the return map
  int n = 1; // Period-1 orbit.
	MpIVector uu0({u0[0], u0[1], MpInterval(0.)});
	MpIVector rr({r[0], r[1], MpInterval(0.)});
  MpC0TripletonSet s0(uu0,rr);

	auto start = high_resolution_clock::now();
  MpIVector fuu0 = pm(s0,n);
  auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start);

	cout << "Initial (x,y,I)      : " << uu0 + rr << endl;
	cout << "diam(x,y,I)          : " << diam(uu0 + rr) << endl;
  cout << "P(x,y,I)             : " << fuu0 << endl;
	cout << "diam(P(x,y,I))       : " << diam(fuu0) << endl;
  cout << "elapsed time         : " << (double) duration.count() / 1000000 << " seconds" << endl;
	cout << "relative error       : " << abs((fuu0[2]-fuu0[2])/fuu0[2]) << endl;

}

int main()
{
  int BITS = 1024;
	MpFloat::setDefaultPrecision(BITS);
	int order;

  // Initial condition, its inflation, and the section.
  MpInterval X0 = MpInterval(9.)/10.;
  MpInterval Y0 = MpInterval(11.)/10.;
  // double  e = 1e-15;
  double  e = 0;

  // section for small ovals
	MpINonlinearSection smallsection ("var:x,y;fun:y-sqrt(1.1);") ;
	MpINonlinearSection smallsection3D ("var:x,y,I;fun:y-sqrt(1.1);") ;

  // section for big ovals
	MpINonlinearSection bigsection ("var:x,y;fun:x;") ;
	MpINonlinearSection bigsection3D ("var:x,y,I;fun:x;");


	// test small oval
	MpInterval R = MpInterval(1.)/2.;
  MpIVector u0({sqrt(X0-R),sqrt(Y0)});
	MpIVector r({ MpInterval (0.), MpInterval(-e,e)});

  BITS = 450;
  order = 110;
  computeAbelianIntegral(u0, r, smallsection3D, order, BITS);




	return 0;


  // big oval
	BITS = 32;
	order = 20;
	R = MpInterval(95.)/100.;
	u0 = MpIVector({MpInterval(0.), sqrt(Y0 + sqrt(R*R-X0*X0))});
	computeAbelianIntegral(u0, r, bigsection3D, order, BITS);

  return 0;
}

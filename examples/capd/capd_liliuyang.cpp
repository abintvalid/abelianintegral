#include <iostream>
#include <chrono>
#include "capd/mpcapdlib.h"
using namespace capd;
using namespace std;
using namespace std::chrono;

// return (x,y,I) with (x,y) the first return and I the value of the Abelian integral
void computeAbelianIntegral(MpIVector u0,
                   MpINonlinearSection section, int order, int BITS, double prec, double step, int n_return)
{
  // adjust floating-point precision
	MpFloat::setDefaultPrecision(BITS);
	cout << "Computing with " << BITS << " bits precision." << endl;
	cout.precision(10);

	// This is vector field for the quartic system
  MpIMap vectorField("par:kappa,lambda,alpha1,alpha2,alpha3,alpha4;var:x,y,I;fun:-y^3+kappa^2*y,x^3+(1-lambda)*x^2-lambda*x,y*(alpha1+alpha2*x+alpha3*x^2+alpha4*y^2)*(y^3-kappa^2*y);");

  // set parameter values
  vectorField.setParameter("kappa", MpInterval(11.)/10.); // κ = 11/10
  vectorField.setParameter("lambda", MpInterval(7.)/10.); // λ = 0.7
  vectorField.setParameter("alpha1", MpInterval(1.));
  vectorField.setParameter("alpha2", MpInterval(1.));
  vectorField.setParameter("alpha3", MpInterval(1.));
  vectorField.setParameter("alpha4", MpInterval(1.));

  cout << "Integrating with order " << order << endl;
  MpIOdeSolver solver(vectorField, order, MpIOdeSolver::StepControlPolicy(1,step));
	solver.setAbsoluteTolerance(prec);
  // solver.setRelativeTolerance(prec);

  // Set up the Poincaré map.
  MpIPoincareMap pm(solver, section, poincare::PlusMinus);

  // Compute the return map
	MpIVector uu0({u0[0], u0[1], MpInterval(0.)});
	MpIVector rr({MpInterval(0.), MpInterval(0.), MpInterval(0.)});
  MpC0TripletonSet s0(uu0,rr);

	cout << "Initial (x,y,I)      : " << uu0 + rr << endl;
	cout << "diam(x,y,I)          : " << diam(uu0 + rr) << endl;

	// transfert initial point onto the transversal
	try{
		auto start = high_resolution_clock::now();
		MpIVector fuu0 = pm(s0,n_return);
		auto stop = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop - start);

	  cout << "P(x,y,I)             : " << fuu0 << endl;
		cout << "diam(P(x,y,I))       : " << diam(fuu0) << endl;
		cout << "relative error       : " << abs((fuu0[2]-fuu0[2])/fuu0[2]) << endl;
		cout << "elapsed time         : " << (double) duration.count() / 1000000 << " seconds" << endl;
	}
	catch(...){
		cout << "FAIL" << endl;
		return;
	}

  return;

}

int main()
{
  int BITS = 512;
	MpFloat::setDefaultPrecision(BITS);
	int order = 5;
	int order_min = 80;
	int order_max = 90;
	int order_step = 2;
	double prec = 1e-132;
	double step = 1e-300;

	// oval 3
	MpInterval lambda = MpInterval(7.)/10.;
	MpInterval kappa = MpInterval(11.)/10.;
	// MpInterval R3max = sqrt(kappa*kappa + 2. * sqrt(-(2. * lambda + 1.) / 12. + kappa*kappa*kappa*kappa / 4.)) - kappa;
	// MpInterval R3 = (R3max * 9.) / 10.;
	MpInterval X0 = MpInterval(-1.395665);
	MpInterval Y0 = MpInterval(1.395665);
	MpIVector u0({X0,Y0});
	MpINonlinearSection section ("var:x,y,I;fun:x+y;");
	for (order = order_min ; order <= order_max ; order += order_step)
  	computeAbelianIntegral(u0, section, order, BITS, prec, step, 1);


	// oval 8
	// MpInterval lambda = MpInterval(7.)/10.;
	// MpInterval kappa = MpInterval(11.)/10.;
	// // MpInterval R8min = sqrt(kappa*kappa + 2. * sqrt(-(2. * lambda + 1.) / 12. + kappa*kappa*kappa*kappa / 4.)) - kappa;
	// // MpInterval R8max = sqrt(kappa*kappa + 2. * sqrt(-lambda*lambda*lambda*(lambda + 2.) / 12. + kappa*kappa*kappa*kappa / 4.)) - kappa;
	// // MpInterval R8 = (R8min + R8max) / 2.;
	// // MpInterval X0 = MpInterval(0.);
	// // MpInterval Y0 = MpInterval(kappa + R8);
	// MpInterval X0 = MpInterval(-1.434261);
	// MpInterval Y0 = MpInterval(1.434261);
	// MpIVector u0({X0,Y0});
	// MpINonlinearSection section ("var:x,y,I;fun:x+y;");
	// for (order = order_min ; order <= order_max ; order += order_step)
	// 	computeAbelianIntegral(u0, section, order, BITS, prec, step, 2);

	return 0;

}

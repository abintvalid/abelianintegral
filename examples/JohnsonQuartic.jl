
using IntervalArithmetic, ApproxFun, RigorousFourier, AbelianIntegral, Plots, LaTeXStrings

using AbelianIntegral: X, Y, PolyType
using IntervalArithmetic: Interval
using ApproxFun: (..)

RigorousFourier.set_default_len(256)
const T = BigFloat
RigorousFourier.set_default_RTA_fastmul(true)
RigorousFourier.set_default_usepow2(true)
setprecision(T, 256)
const IT = Interval{T}
T(x::IT) = mid(x)
### Tomas Johnson's symmetric quatic system

X0 = IT(9)/IT(10)
Y0 = IT(11)/IT(10)

TJpot = (X^2 - X0)^2 + (Y^2 - Y0)^2
TJVX = -4 * Y^2 * (Y^2 - Y0)
TJVY =  4 * X * Y * (X^2 - X0)
TJΣ = 1 / Y
H = PolynomialHamiltonian(TJpot, TJVX, TJVY, TJΣ)

# small oval
smallov = OvalFamily(sqrt(X0), sqrt(Y0), IT(π), IT(0), sqrt(X0), 1)

# big oval
bigov = OvalFamily(IT(0), sqrt(Y0), IT(π)/2, IT(0), IT(∞), 1)

# our perturbation
α00 = IT(-78622148667854837664)/IT(100000000000000000000)
α20 = IT(87723523612653436051)/IT(100000000000000000000)
α22 = IT(1)
α40 = IT(23742713894293038223)/IT(100000000000000000000)
α04 = IT(-21823846173078863753)/IT(100000000000000000000)
pert = PolynomialPerturbation(H, 1, convert(Vector{PolyType}, [IT(0)*X]), convert(Vector{PolyType}, [[α00, α20, α40, α22, α04]' * [1,X^2,X^4,X^2*Y^2,Y^4]]))


# test small oval

R = IT(1)/IT(2)
r = smallov.x0 - sqrt(X0-R)
h = ovallevel(H, smallov, r)
RigorousFourier.set_default_RTA_fastmul_interval(true)
@time I = abelianintegral_valid(T, H, smallov, pert, r, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)

# test large oval
R = IT(95)/IT(100)
r = sqrt(Y0 + sqrt(R^2 - X0^2)) - bigov.y0
h = ovallevel(H, bigov, r)
RigorousFourier.set_default_len(180)
@time I = abelianintegral_valid(T, H, bigov, pert, r, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)
diam.(I)


# rigorous evaluations on small ovals
RigorousFourier.set_default_usepow2(false)
RigorousFourier.set_default_RTA_fastmul(true)
RigorousFourier.set_default_RTA_fastmul_interval(true)
# same results obtained with default_RTA_fastmul_interval = true
# but longer computatation time

# eval point 1: R=0.5, h=0.25
R1 = IT(1)/IT(2)
h1 = R1^2
r1 = smallov.x0 - sqrt(X0-R1)
RigorousFourier.set_default_len(35)
@time I1 = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]

# eval point 2: R=0.78, h=0.6084
R2 = IT(78)/IT(100)
h2 = R2^2
r2 = smallov.x0 - sqrt(X0-R2)
RigorousFourier.set_default_len(75)
@time I2 = abelianintegral_valid(T, H, smallov, pert, r2, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]

# eval point 3: R=0.88, h=0.7744
R3 = IT(88)/IT(100)
h3 = R3^2
r3 = smallov.x0 - sqrt(X0-R3)
RigorousFourier.set_default_len(187)
@time I3 = abelianintegral_valid(T, H, smallov, pert, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]

# eval point 4: R=0.89, h=0.7921
R4 = IT(89)/IT(100)
h4 = R4^2
r4 = smallov.x0 - sqrt(X0-R4)
RigorousFourier.set_default_len(201)
@time I4 = abelianintegral_valid(T, H, smallov, pert, r4, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]

# eval point 5: R=0.895, h=0.801025
R5 = IT(895)/IT(1000)
h5 = R5^2
r5 = smallov.x0 - sqrt(X0-R5)
RigorousFourier.set_default_len(205)
@time I5 = abelianintegral_valid(T, H, smallov, pert, r5, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]

# eval point 6: R=0.8987, h=0.80766169
R6 = IT(8987)/IT(10000)
h6 = R6^2
r6 = smallov.x0 - sqrt(X0-R6)
RigorousFourier.set_default_len(222)
@time I6 = abelianintegral_valid(T, H, smallov, pert, r6, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]


# rigorous evaluations on big ovals

# eval point 7: R=0.901, h=0.811801
R7 = IT(901)/IT(1000)
h7 = R7^2
r7 = sqrt(Y0 + sqrt(R7^2 - X0^2)) - bigov.y0
RigorousFourier.set_default_len(395)
@time I7 = abelianintegral_valid(T, H, bigov, pert, r7, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]

# eval point 8: R=0.93, h=0.8649
R8 = IT(93)/IT(100)
h8 = R8^2
r8 = sqrt(Y0 + sqrt(R8^2 - X0^2)) - bigov.y0
RigorousFourier.set_default_len(257)
@time I8 = abelianintegral_valid(T, H, bigov, pert, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]

# eval point 9: R=0.95, h=0.9025
R9 = IT(95)/IT(100)
h9 = R9^2
r9 = sqrt(Y0 + sqrt(R9^2 - X0^2)) - bigov.y0
RigorousFourier.set_default_len(281)
@time I9 = abelianintegral_valid(T, H, bigov, pert, r9, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1]

# more precise evaluations
RigorousFourier.set_default_usepow2(true)
RigorousFourier.set_default_RTA_fastmul(true)
RigorousFourier.set_default_RTA_fastmul_interval(true)
RigorousFourier.set_default_len(1024)
@time II1 = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]
@time II2 = abelianintegral_valid(T, H, smallov, pert, r2, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]
@time II3 = abelianintegral_valid(T, H, smallov, pert, r3, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]
@time II4 = abelianintegral_valid(T, H, smallov, pert, r4, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]
@time II5 = abelianintegral_valid(T, H, smallov, pert, r5, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]
@time II6 = abelianintegral_valid(T, H, smallov, pert, r6, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]
@time II7 = abelianintegral_valid(T, H, bigov, pert, r7, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]
@time II8 = abelianintegral_valid(T, H, bigov, pert, r8, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]
@time II9 = abelianintegral_valid(T, H, bigov, pert, r9, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1]

# plots
Npts = 200

function plotAbIntSmall(hmin, hmax, Npts, N)
  hh = LinRange(hmin, hmax, Npts)
  rr = (h -> smallov.x0 - sqrt(X0-sqrt(h))).(hh)
  RigorousFourier.set_default_usepow2(true)
  RigorousFourier.set_default_len(N)
  II = (r -> abelianintegral_trapez(T, H, smallov, pert, r, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-30, denoising=1.)[1]).(rr)
  plot([hmin, hmax], [0, 0],
    lw=1,
    lc=:black,
    legend=:none,
    xlabel=L"h",
    ylabel=L"\mathcal{I}(h)")
  plot!(hh, II,
    lw=2,
    lc=:dodgerblue3)
end

function plotAbIntBig(hmin, hmax, Npts, N)
  hh = LinRange(hmin, hmax, Npts)
  rr = (h -> sqrt(Y0 + sqrt(h - X0^2)) - bigov.y0).(hh)
  RigorousFourier.set_default_usepow2(true)
  RigorousFourier.set_default_len(N)
  II = (r -> abelianintegral_trapez(T, H, bigov, pert, r, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-30, denoising=1.)[1]).(rr)
  plot([hmin, hmax], [0, 0],
    lw=1,
    lc=:black,
    legend=:none,
    xlabel=L"h",
    ylabel=L"\mathcal{I}(h)")
  plot(hh, II,
    lw=2,
    lc=:forestgreen)
end

function plotZoomBoxSmall!(hmin, hmax, ymin, ymax)
  plot!(Shape([hmin, hmin, hmax, hmax], [ymax, ymin, ymin, ymax]);
    lc=:dodgerblue3,
    fillcolor=:dodgerblue3,
    fillopacity=0.5)
end

function plotZoomBoxBig!(hmin, hmax, ymin, ymax)
  plot!(Shape([hmin, hmin, hmax, hmax], [ymax, ymin, ymin, ymax]);
    lc=:forestgreen,
    fillcolor=:forestgreen,
    fillopacity=0.5)
end

function plotInterval!(h, I)
  plot!([h, h], [I.lo, I.hi],
    lc=:orangered,
    lw=1,
    legend=:none)
  scatter!([h, h], [I.lo, I.hi],
    markercolor=:orangered,
    markerstrokecolor=:orangered,
    legend=:none)
end

# plot small oval
hmin = T(0.001)
hmax = T(0.809)
plotAbIntSmall(hmin, hmax, 200, 128)
plotZoomBoxSmall!(0.772, 0.808, -0.00001, 0.00001)
plotInterval!(T(h1), I1)
plotInterval!(T(h2), I2)
savefig("JohnsonQuartic-abint-small.pdf")


# plot small oval -- zoom
hmin = T(0.772)
hmax = T(0.808)
plotAbIntSmall(hmin, hmax, 200, 128)
plotInterval!(T(h3), I3)
plotInterval!(T(h4), I4)
plotInterval!(T(h5), I5)
plotInterval!(T(h6), I6)
savefig("JohnsonQuartic-abint-small-zoom.pdf")

# plot big oval
hmin = T(0.811)
hmax = T(0.91)
plotAbIntBig(hmin, hmax, 200, 256)
plotZoomBoxBig!(0.811, 0.815, -0.00003, 0.00003)
plotInterval!(T(h8), I8)
plotInterval!(T(h9), I9)
savefig("JohnsonQuartic-abint-big.pdf")

# plot big oval -- zoom
hmin = T(0.811)
hmax = T(0.815)
plotAbIntBig(hmin, hmax, 200, 256)
plotInterval!(T(h7), I7)
savefig("JohnsonQuartic-abint-big-zoom.pdf")


## compute monomial perturbations
monopert = PolynomialPerturbation(H, 5, convert(Vector{PolyType}, fill( IT(0)*X, 5)), convert(Vector{PolyType},  [1,X^2,X^4,X^2*Y^2,Y^4]))
R1 = IT(1)/IT(2)
h1 = R1^2
r1 = smallov.x0 - sqrt(X0-R1)
RigorousFourier.set_default_len(256)
@time III1 = abelianintegral_valid(T, H, smallov, monopert, r1, iterprec=1e-10, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)


## timings, for h=0.25

# redefine parameters with very high precision
setprecision(T, 1024)
X0 = IT(9)/IT(10)
Y0 = IT(11)/IT(10)
TJpot = (X^2 - X0)^2 + (Y^2 - Y0)^2
TJVX = -4 * Y^2 * (Y^2 - Y0)
TJVY =  4 * X * Y * (X^2 - X0)
TJΣ = 1 / Y
H = PolynomialHamiltonian(TJpot, TJVX, TJVY, TJΣ)
smallov = OvalFamily(sqrt(X0), sqrt(Y0), IT(π), IT(0), sqrt(X0), 1)
bigov = OvalFamily(IT(0), sqrt(Y0), IT(π)/2, IT(0), IT(∞), 1)
α00 = IT(-78622148667854837664)/IT(100000000000000000000)
α20 = IT(87723523612653436051)/IT(100000000000000000000)
α22 = IT(1)
α40 = IT(23742713894293038223)/IT(100000000000000000000)
α04 = IT(-21823846173078863753)/IT(100000000000000000000)
pert = PolynomialPerturbation(H, 1, convert(Vector{PolyType}, [IT(0)*X]), convert(Vector{PolyType}, [[α00, α20, α40, α22, α04]' * [1,X^2,X^4,X^2*Y^2,Y^4]]))
R1 = IT(1)/IT(2)
h1 = R1^2
r1 = smallov.x0 - sqrt(X0-R1)

RigorousFourier.set_default_RTA_fastmul(true)
RigorousFourier.set_default_RTA_fastmul_interval(false)
RigorousFourier.set_default_usepow2(true)

III = IT(0)

# target relative error = 1e-4
setprecision(T, 53)
RigorousFourier.set_default_len(55)
@time for i=1:10 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-10, itermethod="polar", newtoniterations=4, maxdefect=1e-10, denoising=1.)[1] end
abs((III-III) / (2*III))

# target relative error = 1e-6
setprecision(T, 53)
RigorousFourier.set_default_len(70)
@time for i=1:10 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-10, itermethod="polar", newtoniterations=6, maxdefect=1e-10, denoising=1.)[1] end
abs((III-III) / (2*III))

# target relative error = 1e-8
setprecision(T, 128)
RigorousFourier.set_default_len(100)
@time for i=1:10 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-10, itermethod="polar", newtoniterations=6, maxdefect=1e-15, denoising=1.)[1] end
abs((III-III) / (2*III))

# target relative error = 1e-16
setprecision(T, 128)
RigorousFourier.set_default_len(150)
@time for i=1:10 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-15, itermethod="polar", newtoniterations=6, maxdefect=1e-30, denoising=1.)[1] end
abs((III-III) / (2*III))

# target relative error = 1e-24
setprecision(T, 128)
RigorousFourier.set_default_len(225)
@time for i=1:1 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-15, itermethod="polar", newtoniterations=6, maxdefect=1e-30, denoising=1.)[1] end
abs((III-III) / (2*III))
@time abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-15, itermethod="polar", newtoniterations=6, maxdefect=1e-30, denoising=1.)
@time abelianintegral_trapez(T, H, smallov, pert, r1, iterprec=1e-15, itermethod="polar", newtoniterations=6, maxdefect=1e-30, denoising=1.)

# target relative error = 1e-32
setprecision(T, 256)
RigorousFourier.set_default_len(275)
@time for i=1:1 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-40, denoising=1.)[1] end
abs((III-III) / (2*III))

# target relative error = 1e-48
setprecision(T, 256)
RigorousFourier.set_default_len(425)
@time for i=1:1 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-60, denoising=1.)[1] end
abs((III-III) / (2*III))

# target relative error = 1e-64
setprecision(T, 512)
RigorousFourier.set_default_len(570)
@time for i=1:1 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-70, denoising=1.)[1] end
abs((III-III) / (2*III))
@time abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-70, denoising=1.)
@time abelianintegral_trapez(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-70, denoising=1.)

# target relative error = 1e-96
setprecision(T, 512)
RigorousFourier.set_default_len(870)
@time for i=1:1 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-100, denoising=1.)[1] end
abs((III-III) / (2*III))

# target relative error = 1e-128
setprecision(T, 512)
RigorousFourier.set_default_len(1165)
@time for i=1:1 III = abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-140, denoising=1.)[1] end
abs((III-III) / (2*III))
@time abelianintegral_valid(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-140, denoising=1.)
@time abelianintegral_trapez(T, H, smallov, pert, r1, iterprec=1e-20, itermethod="polar", newtoniterations=10, maxdefect=1e-140, denoising=1.)

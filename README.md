# AbelianIntegral

Rigorous evaluation of Abelian integrals for Hilbert 16th
problem.

This package implements the algorithms presented in
**[[1]](#BrehardBrisebarreJoldesTucker2023)**.


## Installation

```julia
julia> ]
(v1.7) pkg> add https://gitlab.inria.fr/abintvalid/abelianintegral
```

This package depends on:

* [`ApproxFun.jl`](https://github.com/JuliaApproximation/ApproxFun.jl) –
Package for approximating functions, in a similar vein to the Matlab package `Chebfun` and the Mathematica package `RHPackage`.

* [`RigorousFourier.jl`](https://gitlab.inria.fr/abintvalid/rigorousfourier) –
Routines for Rigorous Trigonometric Approximations (RTAs).

* [`IntervalArithmetic.jl`](https://github.com/JuliaIntervals/IntervalArithmetic.jl) –
Validated Numerics in Julia, i.e. rigorous computations with finite-precision floating-point arithmetic.

* [`LinearAlgebra.jl`](https://github.com/JuliaLang/julia/tree/master/stdlib/LinearAlgebra) –
Linear algebra routines.

* [`TypedPolynomials.jl`](https://github.com/JuliaAlgebra/TypedPolynomials.jl) —
Multivariate polynomials using strongly typed variables.

* [`DifferentialEquations.jl`](https://github.com/SciML/DifferentialEquations.jl) —
Suite for numerically solving differential equations.

* [`DataStructures.jl`](https://github.com/JuliaCollections/DataStructures.jl) —
Implementation of a variety of data structures.

* [`Reexport.jl`](https://github.com/simonster/Reexport.jl)


## What is an Abelian integral?

Abelian integrals notably occur in the infinitesimal Hilbert 16th problem, i.e., locating limit cycles of polynomial perturbed integrable systems.

A perturbed integrable system is given by a bivariate polynomial or function $H(x,y)$ (the Hamiltonian),
a rescaling factor $\sigma(x,y)$,
and a polynomial $\epsilon$-perturbation $(P,Q)$:

$$
\left\{\begin{aligned}
\dot x &= -\sigma(x,y) \frac{\partial H}{\partial y}(x,y)
+ \epsilon P(x,y), \\
\dot y &= \phantom{-} \sigma(x,y) \frac{\partial H}{\partial x}(x,y)
+ \epsilon Q(x,y).
\end{aligned}\right.
$$

For a given $h$, a closed curve $\Gamma_h$ (an *oval*)
of the level set $H(x,y)=h$
is a periodic orbit of the unperturbed system
($\epsilon=0$). The first order impact in $\epsilon$
of the perturbation $\epsilon (P,Q)$ on the trajectory
is measured by the Abelian integral:

$$
I(h) = \int_{\Gamma_h} \frac{P(x,y) dy - Q(x,y) dx}{\mu(x,y)}.
$$

The algorithms below aim at numerically computing $I(h)$
efficiently and rigorously: the result is an interval containing the exact mathematical value of $I(h)$.  

## Data structures

### Integrable systems

The type `FunctionHamiltonian` represents a Hamiltonian (or integrable) system, with attributes of type function:

  * `pot` for $H(x,y)$
  * `dx` and `dy` for the first derivatives
  $\frac{\partial H}{\partial x}$ and
  $\frac{\partial H}{\partial y}$
  * `dxx`, `dyy` and `dxy` (optional)
  for the second derivatives
  $\frac{\partial^2 H}{\partial x^2}$,
  $\frac{\partial^2 H}{\partial y^2}$,
  $\frac{\partial^2 H}{\partial x \partial y}$
  * `vx` and `vy` for the expression of the vector field
  * `σ` the integrating factor, such that
    `σ` * `vx` = -`dy` and
    `σ` * `vy` = `dx`.

The type `PolynomialHamiltonian` has extra attributes
`Pot`, `DX`, `DY`, `DXX`, `DYY`, `DXV`, `VX`, `VY` and
`Σ` which are polynomial or rational functions in $X$ and $Y$ (the other attributes inherited from `FunctionHamiltonian` begin automatically deduced as the evaluation functions of these polynomial/rational functions).


### Families of ovals

The type `OvalFamily` has attributes `x0`, `y0`, `α`, `rmin`, `rmax` and `orient`. It represents the family of ovals crossing the segment of the half-line starting
from (`x0`, `y0`) with angle `α` w.r.t. the $x$-axis
at distance $r$ to (`x0`, `y0`)
for `rmin` $\leq r $\leq$ `rmax`.

The level value associated to the oval of parameter `r` in the oval family `ov` of Hamiltonian `H` is computed by
`ovallevel(H, ov, r)`.


### Perturbations

The type `FunctionPerturbation` represents perturbation terms $\epsilon (P,Q)$ of the integrable system. Its attributes are:

* `dim`, the number of perturbation terms.
* `wx` and `wy`, the $x$ and $y$ components of the perturbation terms of $(w_x, w_y)$. They are functions from `Real` to
`Vector{Real}` (of length `dim`).
* `green` (optional), the function calculating the Green term
$\frac{\partial (\sigma w_x)}{\partial x} +
\frac{\partial (\sigma w_y)}{\partial y}$.

The type `PolynomialPerturbation` has extra attributes `WX`, `WY` and `Green` of polynomial/rational function type in $X$ and $Y$ to represent the functions `wx`, `wy` and `green`.


## Routines computing the Abelian integrals

### Numerical routines for approximation

* `ovalapprox_iterative(T, H, ov, r, prec=p, method="xxx")`
computes an approximation of the oval characterized by
(`H`, `ov`, `r`) using an iterative solver with floating-point type `T` and target precision `p`. The parameter `method` selects which differential system used to follow the algebraic curve:
  * `"cartesian"`: the standard $(x,y)$ Cartesian form as in the introduction.
  * `"polar"`: the polar form
  $(\dot \rho(t), \dot theta(t)) = f(t, \rho(t), \theta(t))$.
  * `"odepolar"`: the reduced equation
  $\frac{d \rho}{d \theta}(\theta) = g(\theta, \rho(\theta))$.
The result is functions (`x`, `y`, `dx`, `dy`) from `T` to `T` together with a maximum time `tend` corresponding to the first return on the transversal segment represented by `ov`.

* `ovalapprox_TA(T, H, ov, r)` refines the approximation of the Oval using Newton's method and represents the solution by TAs (Trigonometric Approximations) with floating-point coefficients of type `T` for $t \mapsto x(t)$ and $t \mapsto y(t)$. Optional parameters are:
  * `iterprec` and `itermethod` for the call to `ovalapprox_iterative`.
  * `newtoniterations=n` and `maxdefect=d`: stop if the defect is smaller than `d`, or if the maximum number of iterations `k` was reached.
  * `finalfft`: if `true` (default value), returns TAs, otherwise just returns the approximation points.
  * `denoising`: a threshold to cut nonrelevant coefficients of TAs in the initial approximation computed by
  `ovalapprox_iterative`.

* `abelianintegral_trapez(T, H, ov, pert, r)` computes an floating-point approximation of type `T` for the Abelian integral, based on the trapezoidal rule applied to the oval approximation computed by `ovalapprox_TA`. The optional arguments are those of `ovalapprox_TA`.


### Validated numerical routines

* `ovalvalid(H, ov, r, x, y)` takes as input TAs `x`, `y` approximating the oval, and returns TAs `ux`, `uy` defining a "transversal" direction to the oval at any time $t$, and a bound `ϵ` such that there exists a unique exact parameterization
x*, y* of the oval of the form

$$
\left\{\begin{aligned}
x^*(t) &= x(t) + s(t) u_x(t), \\
y^*(t) &= y(t) + s(t) u_y(t),
\end{aligned}\right. \qquad \text{with }
||s||_{\infty} \leq \epsilon.
$$

* `abelianintegral_valid(H, ov, pert, r)` computes an interval that rigorously contains the exact mathematical value of the Abelien integral. Its optional arguments are those of `ovalapprox_TA`.


## Examples

See the directory `examples/`.


## Bibliography

**<a name="BrehardBrisebarreJoldesTucker2023">[1]</a>**
Florent Bréhard, Nicolas Brisebarre, Mioara Joldes and Warwick Tucker, *Efficient and Validated Numerical Evaluation of Abelian
Integrals*, submitted.
